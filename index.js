/* s31 index.js */
// #31….26Apr2022(TUE)
// WDC028-31 Express.js - Modules, Parameterized Routes


// set up of dependencies

/*
	- load express & mongoose packages & store them inside the "express" & "mongoose" 
	variables respectively
	- create "app" variable & store express server inside
	- create a port variable & store 3000 in it
	- make the app able to read json & accept other types of data from forms
	- connect your app to mongodb (use the one we have previously used:"b170-to-do" database)
	- set up confirmation of connecting or not connecting to the database
	- make the app listen to port & set up confirmation in the console that the "server is 
		running at port"
*/

const express = require("express");
const mongoose = require("mongoose");

// load the package that is inside the routes.js file in the repo
const taskRoute = require("./routes/taskRoutes.js")

const app = express();

const port = 3000;



//app.use must be bet. const&mongoose.connect
app.use(express.json()); 
app.use(express.urlencoded({ extended:true})); 

mongoose.connect("mongodb+srv://mhsalanga:Koykoy22042022@wdc028-course-booking.fogfn.mongodb.net/b170-to-do?retryWrites=true&w=majority",
		{
		useNewUrlParser:true,
		useUnifiedTopology:true	
		})

// notification for connection: success/failure
let db = mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error",console.error.bind(console,"Connection Error"))
// if the connection is successful
db.once("open", () => console.log("We're connected to the database"))


// gives the app an access to the routes needed
// this will set the base URI once we want to access the routes under the tasks
app.use("/tasks",taskRoute)




app.listen(port, () => console.log(`Now listening to port ${port}`)); 


// POSTMAN
// B170-s31
// get all tasks  -----> GET ---->  http://localhost:3000/tasks/


