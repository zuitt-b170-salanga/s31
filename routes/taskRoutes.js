/* s31 routes.js */

// routes - contains all the endpoints for the application; instead of putting a lot of 
	//routes in the index.js, we separate them in other file, routes.js (databaseRoutes.js i.e. 
	//taskRoutes.js)

const express = require("express")

// creates a router instance that functions as a middleware & routing system: allows access to 
	//HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();


// allows us to use the controllers' functions
const taskController = require("../controllers/taskControllers.js")

// before, "/" means accessing localhost:3000/
// through the use of app.use("/tasks", taskRoute) in index.js for the taskRoutes, localhost:3000/tasks/


// accessing different schema, assuming we have others, would set up another app.use in the index.js and 
	//other files inside models, routes, and controllers folders
// for example, if you add another schema that is Users, the base uri for that would be set by 
	//app.use("/users", userRoutes) and accessing its "/" url inside userRoutes would be 
	//localhost:3000/users/
// route to get all tasks
router.get("/",(req,res)=>{
	// taskController - a controller file that is existing in the repo
	// getAllTasks is a function that is encoded inside the taskController file
	// use .then to wait for the getAllTasks function to be executed before proceeding to the 
		//statements (res.send(result)
	taskController.getAllTasks().then(result=>res.send(result));
})

// route for creating a task
router.post("/",(req,res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})


/*
	: - wildcard; if tagged w/ a parameter, the app will automatically look for the parameter 
		//that is in the url
*/
// localhost:3000/tasks/6267de2fcf0f3b231b622fc8
router.delete("/:id",(req,res)=>{    // "tasks/:id" -----> "/:id"
	// URL parameter values are accessed through the request object's "params" property; the 
		//specified parameter/property of the object will match the URL parameter endpoint
	taskController.deleteTask(req.params.id).then(result=>res.send(result))
})

router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


// ************************ s31 ****************************
/* 1 */
// route to get a specific task in the db
router.get("/:id",(req,res)=>{	
	taskController.getATask(req.params.id, req.body).then(result=>res.send(result));
})


/* 2 */
// route to update the status of a task
router.put("/:id/complete", (req, res)=>{
	taskController.updateStatusTask(req.params.id, req.body).then(result => res.send(result));
})


// ********************************************************
// Postman:
// B170 -s31
// GET a TASK
// http://localhost:3000/tasks/6262ab72164f6606b0237ab7
// *Output:
// {
//     "_id": "6262ab72164f6606b0237ab7",
//     "name": "sweeping the floor",
//     "status": "pending",
//     "__v": 0
// }

// Postman:
// {
//         "_id": "626677de7a6b35e05db79350",
//         "status": "pending",
//         "__v": 0,
//         "name": "watering the plants"
// }
// B170 -s31
// UPDATE TASK STATUS 
// PUT
// http://localhost:3000/tasks/626677de7a6b35e05db79350/complete
// {
//     "status": "complete"
// }

// *Output:
// {
//     "status": "complete",
//     "_id": "626677de7a6b35e05db79350",
//     "__v": 0,
//     "name": "watering the plants"
// }

// ************************ s31 ****************************


// module.exports - a way for js to treat the value/file as a package that can be imported/used
	//by other files
module.exports = router;
