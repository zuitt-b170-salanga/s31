/* s31 controllers.js */

// containing the functions/statements/logic to be executed once a route has been 
	//triggered/entered
// responsible for execution of CRUD operations/methods.

// to give access to the contents for tasks.js file in the models folder; meaning it can use the 
	//Task model
const Task = require("../models/task.js");


// create controller functions that responds to the routes 
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}





module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	// saving mechanisms
	/*
		.then accepts 2 parameters
			- 1st parameter stores the result object; if we have successfully saved the object
			- 2nd parameter stores the error object, should there be one
	*/
	return newTask.save().then((savedTask, error) => {
		if (error) {
			console.log(error);
			return false
		} else {
			return savedTask
		}
	})
}

/*
	1. look for the task w/ the corresponding id provided in the URL
	2. delete the task

*/
module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - finds the item to be deleted and removes it from the database; it uses id in 
		//looking for the document
	return Task.findByIdAndRemove(taskId).then((removedTask,error) =>{
		if(error){
			console.log(error);
			return false
		} else {
			return removedTask
		}
	})
}

//***********************************************

/*
	update task - 2 parameters
	1. to find the id (findById())
	2. replace the task's name returned from the database w/ the name property of the 
		requestBody
	3. save the task
*/

//***********************************************


module.exports.updateTask = ( taskId, requestBody ) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		} else {
			result.name = requestBody.name;
			return result.save().then((updateTask, error)=>{
				if (error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}

//***********************************************

// ACTIVITY
/*
1 get a specific task in the database
	BUSINESS LOGIC
	"/:id"
	-find the id (findById())
	-return it as a response


2. update the status of a task
	BUSINESS LOGIC
	"/:id/complete"
	- find the id of the task
	- change the value of the status into "complete" using the requestBody
	- save the task	
*/

// ************************ s31 ****************************
/* 1 */
module.exports.getATask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}


/* 2 */
module.exports.updateStatusTask = ( taskId, requestBody ) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		} else {
			result.status = requestBody.status;
			return result.save().then((updateStatusTask, error)=>{
				if (error) {
					console.log(error)
					return false
				}else{
					return updateStatusTask
				}
			})
		}
	})
}

// ************************ s31 ****************************

